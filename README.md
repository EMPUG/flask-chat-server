# flask-chat-server

## Installation

1. Clone the repository

       cd /path/to/your/projects
       git clone https://gitlab.com/EMPUG/flask-chat-server.git

2. Make the repository directory the current directory:

       cd flask-chat-server

3. Install required packages in a virtual environment. On *nix or Mac OS, execute:

       python3 -m venv venv
       source venv/bin/activate
       pip install -r requirements.txt

## Run

* Run the app with the flask dev server

      flask run

* Or run the app with the ip and port defined

      flask run -h 0.0.0.0 -p 8080
from flask import Flask, render_template, request

from model import db, Message


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite+pysqlite:///:memory:'
db.init_app(app)

with app.app_context():
    db.create_all()


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        message = Message()
        message.message_text = request.form.get('msg')
        db.session.add(message)
        db.session.commit()

    messages = db.session.execute(db.select(Message).order_by(Message.id)).scalars().all()

    return render_template(
        template_name_or_list='index.html',
        messages=messages,
    )


if __name__ == '__main__':
    app.run(debug=True)

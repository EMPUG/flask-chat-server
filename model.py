import datetime

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Message(db.Model):
    __tablename__ = 'message'
    id = db.Column(db.Integer, primary_key=True)
    message_time = db.Column(db.DateTime, default=datetime.datetime.now)
    message_text = db.Column(db.String)
